#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
    // Given
    int valor1 = -4;
    int valor2[] = {2};
    int valor3 = 1;
    int valorExpected = 2;
    
    // When
    int resultado = valuesNumber(valor1, *valor2, valor3);
    
    // Then
    assertEquals_int(valorExpected, resultado);
}
void testExercise_B() {
    // Given
    int valor1 = 22;
    int valor2[] = {22};
    int valor3 = 1;
    int valorExpected = 22;
    
    // When
    int resultado = valuesNumber(valor1, *valor2, valor3);
    
    // Then
    assertEquals_int(valorExpected, resultado);
}
void testExercise_C() {
    // Given
    int valor1 = 62;
    int valor2[] = {62,62};
    int valor3 = 2;
    int valorExpected = 62;
    
    // When
    int resultado = valuesNumber(valor1, *valor2, valor3);
    
    // Then
    assertEquals_int(valorExpected, resultado);
}
void testExercise_D() {
    // Given
    int valor1 = -8;
    int valor2[] = {-8, -8, -4, -8, -8, 62};
    int valor3 = 6;
    int valorExpected = 4;
    
    // When
    int resultado = valuesNumber(valor1, *valor2, valor3);
    
    // Then
    assertEquals_int(valorExpected, resultado);
}
void testExercise_E() {
    // Given

    // When
 
    // Then
	fail("Not yet implemented");
}
