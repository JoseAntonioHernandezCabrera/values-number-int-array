#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jint JNICALL Java_library_valuesNumber_1
  (JNIEnv *env, jobject object, jint foundValue, jintArray value, jint valueLength)
{
    javaEnv = env;
    int c_foundValue = toInt(foundValue);
    int c_valueLength = toInt(valueLength);
    int* c_value = toIntArray(value, c_valueLength);
    int c_outValue = valuesNumber(c_foundValue, c_value, c_valueLength);
    setJintArray(value, c_value, &c_valueLength);
    return toJint(c_outValue);
}

