#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */


int valuesNumber(int foundValue, int* value, int valueLength) {
    int contador = 0;

    for (int i = 0; i < valueLength; i++) {
        if (value[i] == foundValue) {
            contador++;
        }
    }

    return contador;
}

int valuesNumber(int foundValue, int* value, int valueLength) {
    int* oddEvenExchangedValues(int* value, int valueLength){
        if(valueLength == 1){
            return value;
        }
        for(int i = 0; i < valueLength -1; i++){
            int dvr = value[i];
            value[i] = value[i+1];
            value[i+1] = dvr;
        }
    }
    return value;
}
